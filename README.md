# 使用本範例

請在 php/data/ 內加入 `.gitignore` 檔案

內容為
```
*
```
意思為 git 忽略該資料夾所有資料

記住 本範例僅為展示 所以將 conf 展出, 自己使用時請勿將 conf 暴露

編碼和密碼也在裡面.... 所以能夠反轉出你的密碼是什麼

`php/data` 內請自行用 `sftp` 的方式 送到 `openshift/app-root/data` 目錄

`init 0aa6a9b` 版本 需要 `symbolic link` 來綁定

目前版本 移除 `symbolic link`

在 `php/inc/init.php` 中 使用 if else condition 來確定環境並切換 conf path

在 `php/data/conf` 中 也使用 if 來切換 save_dir 路徑

OK 廢話這麼多 總之~


- 在 `php/data/` 目錄加入 `.gitignore` 檔案內容上面有寫
- `git push` 到 `openshift gear`
- 用 sftp 把 `php/data/conf` and `php/data/data` 兩個資料夾送到 `openshift/app-root/data` 底下
- 帳號 `test` 密碼 `test` 登入看看
- 記得改密碼, 享受你的 `dokuwiki` 之旅吧

